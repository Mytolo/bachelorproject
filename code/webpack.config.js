// Copyright 2019, Panajiotis Christoforidis <panajiotis@christoforidis.net>
// SPDX-ID: GPL-3.0-or-later

const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const WebpackBar = require('webpackbar')
const CopyPlugin = require('copy-webpack-plugin')
const ModuleReplaceWebpackPlugin = require('module-replace-webpack-plugin')
const package = require('./package.json')


const proxyPath = path.resolve(__dirname, 'eaas-proxy')

module.exports = {
    entry: './src/index.js',
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: require.resolve('@open-wc/webpack-import-meta-loader'),
            },
            {
                test: /\.(js)$/,
                use: ['babel-loader']
            },
            {
                test: /\.scss/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    externals: {
        // net: "true",
        fs: "true",
        dns: "true",
        child_process: 'true'
    },
    plugins: [
       //  new CriticalCssPlugin({
       //      'src': 'src/index.html',
       //      'dest': 'dist/index.html'
       //  }),
       // new ModuleReplaceWebpackPlugin({
       //     modules: [{
       //         test: /net/,
       //         replace: 'src/lib/net.js'
       //     }],
       //     exclude: [/net.js$/]
       // }),
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            title: package.title || package.name,
	    favicon: "./src/static/favicon.jpg"

        }),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: 'hallo.css'
        }),
        new CopyPlugin([
            { from: 'eaas-proxy/picotcp.js/picotcp.wasm', to: 'eaas-proxy/picotcp.js/picotcp.wasm' },
        ]),
        new webpack.ProvidePlugin(
            {
                process : path.resolve(__dirname, 'src/lib/process.js')
            }
        ),
        new WebpackBar({profile: true}),
        // new webpack.debug.ProfilingPlugin({
        //     outputPath: 'profilingEvents.json'
        // })
    ],
    resolve: {

        alias: {
            "eaas-proxy": path.resolve(__dirname, "eaas-proxy"),
            net: path.resolve(__dirname, 'src/lib/net.js'),
        }
    },
    devServer: {
        open: true
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: (module) => module.resource && module.resource.startsWith(proxyPath),
                    name: "eaas-proxy",
                    chunks: "all"
                },
                styles: {
                    name: 'styles',
                    test: /\.css$/,
                    chunks: 'all',
                    enforce: true,
                }
            }
        }
    }
}
