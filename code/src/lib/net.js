// Copyright 2019, Panajiotis Christoforidis <panajiotis@christoforidis.net>
// SPDX_ID-ID: GPL-3.0-or-later



// Reference: [Nodejs' Net Module](https://nodejs.org/api/net.html)
const { Duplex } = require('stream')


// This class implements Socket as they are in Nodejs and connects a nodejs application stream to the host tcp network
// via webstreams
export class Socket extends Duplex {
    // pass default arguments to the constructor of Duplex class
    constructor(options) {
        super({
            autoDestroy: false,
            emitClose: false,
            objectMode: false,
            readableObjectMode: false,
            writableObjectMode: false,
            ...options
        })
    }

    setNoDelay() {
        console.log('setNoDelay called')
    }

    ref() {
        return this
    }

    setTimeout() {
        console.log('setTimeout called')
    }

    // connect in the fashion of the [net.connect](https://nodejs.org/api/net.html#net_net_connect)
    // method from nodejs' API
    async connect(options) {
        // conventions needed by node
        this.connecting = true
        const {host, port} = options
        console.log(`connect to ${host}:${port}`)

        // socket connection will be established
        // open socket to virtual network
        this.socket = new global.picoTcpNIC.TCPSocket(host, port);
        this.writer = this.socket.writable.getWriter();

        // push incoming data on the socket into this stream
        (async () => {
            for await (const buffer of this.socket.readable) {
                this.push(buffer)
            }
        })()

        // for now, directly emit connect.
        // since the connection is established the api triggers a connect event
        this.emit('connect')
        this.connecting = false
        // TODO(mytolo): Investigate whether this is wanted
        return this
    }

    // destroy method, this could be the place where the streams have to be cleaned
    _destroy (err, callback) {
        /* if (ws.readyState === ws.CLOSED) {
            callback(err);
            process.nextTick(emitClose, duplex);
            return;
        }

        ws.once('close', function close() {
            callback(err);
            process.nextTick(emitClose, duplex);
        });
        ws.terminate(); */
    };

    _final (callback) {
        /*if (ws.readyState === ws.CONNECTING) {
            ws.once('open', function open() {
                duplex._final(callback);
            });
            return;
        }

        if (ws._socket._writableState.finished) {
            if (duplex._readableState.endEmitted) duplex.destroy();
            callback();
        } else {
            ws._socket.once('finish', function finish() {
                // `duplex` is not destroyed here because the `'end'` event will be
                // emitted on `duplex` after this `'finish'` event. The EOF signaling
                // `null` chunk is, in fact, pushed when the WebSocket emits `'close'`.
                callback();
            });
            ws.close();
        } */
    };

    // enable reading from socket
    _read (size) {
        /* if (ws.readyState === ws.OPEN && !resumeOnReceiverDrain) {
            resumeOnReceiverDrain = true;
            if (!ws._receiver._writableState.needDrain) ws._socket.resume();
        } */
}

    // enable writing into socket
    _write (chunk, encoding, callback)  {
        // push data into socket
        this.writer.write(chunk).then(() => callback(), callback);

        /* if (ws.readyState === ws.CONNECTING) {
            ws.once('open', function open() {
                duplex._write(chunk, encoding, callback);
            });
            return;
        }

        ws.send(chunk, callback);
         */
    }


    // find the piece of code where the streams of the socket has to be cleaned peacefully.
    duplexOnEnd() {
        if (!this.destroyed && this._writableState.finished) {
            this.destroy();
        }
    }


    // check whether this is really necessary, the error stack should kept working
    duplexOnError(err) {
        this.removeListener('error', this.duplexOnError);
        this.destroy();
        if (this.listenerCount('error') === 0) {
            // Do not suppress the throwing behavior.
            this.emit('error', err);
        }
    }
}

export let createConnection = (options) => {
    let sock = Socket(options)
    sock.connect(options)
    return sock
}



