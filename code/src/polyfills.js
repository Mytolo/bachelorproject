// Copyright 2019, Panajiotis Christoforidis <panajiotis@christoforidis.net>
// SPDX-ID: GPL-3.0-or-later



// (Incomplete) polyfill for <https://streams.spec.whatwg.org/#rs-get-iterator>
ReadableStream.prototype[Symbol.asyncIterator] = async function* () {
    const reader = this.getReader();
    reader.next = reader.read;
    // globalThis.a = reader;

    try {
        for await (const value of reader) yield value;
    } finally {
        reader.cancel();
    }
};