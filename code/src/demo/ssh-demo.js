// Copyright 2019, Panajiotis Christoforidis <panajiotis@christoforidis.net>
// SPDX_ID-ID: GPÖLL-3.0-or-later



import {Client} from "ssh2";

import {DEBUG, APPLICATION_HOST, APPLICATION_PORT, WEBSOCKET_HOST, WEBSOCKET_PORT, SSH_PW, SSH_USER} from "../constants";
import {addLog} from "../lib/logger";
import {integrateWebSocketStreamIntoNetwork} from "../lib/network";

function testSSH2OnWebStream(cmd = 'uptime') {
    const conn = new Client();
    conn.on('ready', () => {
        console.log('Client :: ready');
        console.log(`executing ${cmd}`)
        conn.shell((err, stream) => {
            Object.assign(globalThis, {writer: stream})
            if (err) throw err;
            stream.on('close', (code, signal) => {
                console.log('Stream :: close :: code: ' + code + ', signal: ' + signal);
                conn.end()
            }).on('data', (data) => {
                addLog(`${data}`, 'STDOUT');
            }).stderr.on('data', (data) => {
                addLog(`${data}`, 'STDERR');
            });
        });

    }).connect({
        host: APPLICATION_HOST,
        port: APPLICATION_PORT,
        username: SSH_USER,
        password: SSH_PW,
        debug: (msg) => DEBUG ? addLog(msg, 'ssh2-DEBUG') : true
    });
}

// If you want to integrate your network application to the local tcp network via webstreams,
// you can use integrateWebSocketStreamIntoNetwork

const run = async (cmd, network) => {

    // make SSH_PROXY_HOST:SSH_PROXY_PORT (from host network) available as SSH_HOST:PORT in virtual network
    // This is achieved by connecting the virtual network through a websocat proxy onto the local host socket.
    integrateWebSocketStreamIntoNetwork(
        WEBSOCKET_HOST,
        WEBSOCKET_PORT,
        network.nic1,
        APPLICATION_PORT
    );

    // try using ssh2 library on it
    testSSH2OnWebStream(cmd)
}

export default run
