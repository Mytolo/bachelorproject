#!/usr/bin/env bash


if [ "x$3" == "x" ]
then
  echo -e "port-to-websocket.sh <ip-Addr of Server> <Service Port> <Websocket Port>"
  exit 1
fi

# do not use this version outside of a docker container !
# instead use:
# websocat -vvv -E -b ws-l:127.0.0.1:$3 tcp:$1:$2
websocat -vvv -E -b ws-l:0.0.0.0:$3 tcp:$1:$2
