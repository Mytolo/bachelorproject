apt-get update && apt-get install -y apt-utils
apt-get install -y openssh-server curl build-essential libssl-dev pkg-config
curl https://sh.rustup.rs -o rs.sh -sSf
bash rs.sh -y
# export PATH=$HOME/.cargo/bin:$PATH
$HOME/.cargo/bin/cargo install --features=ssl websocat