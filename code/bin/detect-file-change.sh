#!/usr/bin/env bash
#
# Provides      : Check if a file is changed
# 
# Limitations   : none
# Options       : none
# Requirements  : bash, md5sum, cut
# 
# Modified      : 11|07|2014
# Author        : ItsMe
# Reply to      : n/a in public
#
# Editor        : panajiotis christoforidis
#
#####################################
#
# OK - lets work
#


if [ "x$1" == "x" ]
then
  echo -e "USAGE: monitorFileChange <file>"
  exit 1
fi


file=$1


changed() {
fingerprintfile=/tmp/$(basename $file).md5sum

# does the file exist?
if [ ! -f $file ]
    then
        echo "ERROR: $file does not exist - aborting"
    exit 1
fi

# create the md5sum from the file to check
filemd5=`md5sum $file | cut -d " " -f1`

# check the md5 and
# show an error when we check an empty file
if [ -z $filemd5 ]
    then
        echo "The file is empty - aborting"
	echo $filemd5 > $fingerprintfile
        exit 1
    else
        # pass silent
        :
fi

# do we have allready an saved fingerprint of this file?
if [ -f $fingerprintfile ]
    then
        # yup - get the saved md5
        savedmd5=`cat $fingerprintfile`

        # check again if its empty
        if [ -z $savedmd5 ]
            then
                echo "The file is empty - aborting"
		echo $filemd5 > $fingerprintfile
                exit 1
        fi

        #compare the saved md5 with the one we have now
        if [ "$savedmd5" == "$filemd5" ]
            then
                # pass silent
                echo $filemd5 > $fingerprintfile
		exit 0
                :
            else
                echo "1"

                # this does an beep on your pc speaker (probably)
                # you get this character when you do:
                # CTRL+V CTRL+G
                # this is a bit creepy so you can use the 'beep' command
                # of your distro
                # or run some command you want to
                echo $filemd5 > $fingerprintfile
		exit 0
        fi
    else
        echo $filemd5 > $fingerprintfile

fi

}
changed
