#!/usr/bin/env bash

websocat -vvvv -E -b ws-l:127.0.0.1:$3 tcp:$1:$2
