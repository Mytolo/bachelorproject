const fs = require('fs')
const net = require('net')
const ssh2 = require('ssh2')
const WebSocket = require('ws')

const path = require('path')

//config
const SSH_PORT = 22
const SSH_HOST = 'localhost'

/*
// test file write Stream
const stream = net.connect({
  port: SSH_PORT,
  host: SSH_HOST
})
*/

const ws = new WebSocket("ws://localhost:2122")
const stream = WebSocket.createWebSocketStream(ws)



const readline = require('readline');
const Writable = require('stream').Writable;

const mutableStdout = new Writable({
  write: function(chunk, encoding, callback) {
    if (!this.muted)
      process.stdout.write(chunk, encoding);
    callback();
  }
});

mutableStdout.muted = false;

const rl = readline.createInterface({
  input: process.stdin,
  output: mutableStdout,
  terminal: true
});

rl.question('Password: ', function(password) {
  rl.close();
  const conn = new ssh2.Client();
  conn.on('ready', () => {
    console.log('Client :: ready');
    conn.exec('uptime', (err, stream) => {
      if (err) throw err;
      stream.on('close', (code, signal) => {
        console.log('Stream :: close :: code: ' + code + ', signal: ' + signal);
        conn.end();
      }).on('data', (data) => {
        console.log('STDOUT: ' + data);
      }).stderr.on('data', (data) => {
        console.log('STDERR: ' + data);
      });
    });
  }).connect({
    sock: stream,
    host: SSH_HOST,
    port: SSH_PORT,
    username: 'christop',
    privateKey: fs.readFileSync(path.resolve(process.env.HOME, '.ssh/id_rsa')),
    passphrase: password
  });
});

mutableStdout.muted = true;




