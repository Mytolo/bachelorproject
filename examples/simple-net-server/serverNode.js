const net = require('net');


(async () => {
const server = await net.createServer((c) => {
  // 'connection' listener.
  socket = c
  console.log('client connected')
  c.on('end', () => {
    console.log('client disconnected')
  })
  c.write('This is a standard net-server from Nodejs listening on port 1234\r\n')
  c.on('data', (data) => {
    console.log(data)
    console.log(data.toString())
    c.write(data.toString())
  })
  // c.pipe(c)
});
server.on('error', (err) => {
  throw err
});
server.listen(1234, () => {
  console.log('server bound')
})

})()

// (async () => {
// let client = net.connect({host: '127.0.0.1', port: 1234}, () => {
//   console.log('client connected successfully')
//   client.write('hello\r\n')
//   client.end('bye\r\n')
// })
// 
// client.on('data', (data) => {
//   console.log(data.toString())
// })
// 
// })()
