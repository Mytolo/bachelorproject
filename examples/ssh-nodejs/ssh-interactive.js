const Client = require('ssh2').Client
const fs = require('fs')
 
var conn = new Client();
conn.on('ready', function() {
  console.log('Client is ready')
  conn.shell(function(err, stream) {
    stream.write('uptime\r')
    console.log(typeof(stream))
    if (err) throw err;
    stream.on('close', function() {
      console.log('Stream closed')
      process.stdin.unpipe(stream)
      conn.end()
    }).on('data', function(data) {
      console.log('OUTPUT: ' + data)
    })
    process.stdin.pipe(stream)
  })
}).connect({
  host: '192.168.2.107',
  port : 22,
  username: 'christop',
  privateKey: fs.readFileSync('/home/christop/.ssh/id_rsa'),
  passphrase: 'passphrase'
})


